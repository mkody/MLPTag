# MLPTag
![Build](http://git.kdy.ch/MKody/MLPTag/badges/master/build.svg) [![js-standard-style](https://img.shields.io/badge/code%20style-standard-green.svg)](http://standardjs.com/)

A bot that reads a Derpibooru RSS file and posts whats is in his "Watch List"  
_May use the Derpibooru API someday. Maaaaaaybe._

### How to run ?
- Create a Derpibooru acount for your bot and make it follow some tags (use correct filters too)
- Create a Twitter app using your Twitter bot account. Add write permission and create a token
- `git clone http://git.kdy.ch/MKody/MLPTag.git && cd MLPTag`
- `npm install`
- `cp config.json.example config.json`
- `$EDITOR config.json`
- Edit the file to your needs
	+ **latest**: don't touch that
	+ **rss_url**: your watch url
	+ **twitter**: add your api key and the user token to post to
	+ **future_ep**: array of tags that will have the `#spoilers` tag
- add a cron to run `node rss_app.js` properly (direct output to a `cron.log` file, and logrotate this file)
