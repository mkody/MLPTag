var request = require('request')
var fs = require('fs')
var parseString = require('xml2js').parseString
var Twit = require('twit')

// Check if config exist
try {
  fs.accessSync('config.json', fs.F_OK)
} catch (e) {
  console.error('Config.json doesn\'t exist!')
  process.exit(1)
}

// Config variables
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'))
var rssUrl = config.rss_url
var futureEp = config.future_ep
var twitter = new Twit({
  consumer_key: config.twitter.key,
  consumer_secret: config.twitter.secret,
  access_token: config.twitter.token,
  access_token_secret: config.twitter.token_secret
})

// function to sort of dates
function compareDates (a, b) {
  var aDate = new Date(a.pubDate)
  var bDate = new Date(b.pubDate)

  if (aDate < bDate) return -1
  if (aDate > bDate) return 1
  return 0
}

// Sync downloading to save images temporarly
function download (uri, filename, callback) {
  request.head(uri, function (err, res, body) {
    request(uri).pipe(fs.createWriteStream('pics/' + filename)).on('close', callback)
    if (err) {
      console.error('Error on download: ' + err)
    }
  })
}

// Let's do all the stuff we need to post our item to Twitter
function publishToTwitter (item) {
  // RegExp and stuff to get URL and an image title
  var re = /<img[^>]+src="https:\/\/([^">]+)/g
  var results = re.exec(item.description[0])
  var imgURL = 'https://' + results[1]
  var imgURLSplit = imgURL.split('/')
  var imgName = imgURLSplit[7] + imgURLSplit[8]
  var postTitle = item.title[0].substr(0, 8)

  // Let's download that file
  download(imgURL, imgName, function () {
    // We need to upload the image first
    console.log('Uploading image "' + imgName + ' to Twitter')
    twitter.post('media/upload',
      {media: fs.readFileSync('pics/' + imgName, { encoding: 'base64' })},
      function (err, data, response) {
        // Things may not work...
        if (err) console.log('Error with ' + postTitle + ': ' + err)

        // If everything's fine and we have the media online
        if (err === null && data.media_id_string) {
          // We check for hashtags to add
          var twtprefix = ''
          if (new RegExp(futureEp.join('|')).test(item.title[0])) {
            twtprefix = '#Spoilers '
          }
          if (item.title[0].indexOf('spoiler:s06') > -1) {
            twtprefix = twtprefix + '#MLPSeason6 '
          }
          if (item.title[0].indexOf('spoiler:s07') > -1) {
            twtprefix = twtprefix + '#MLPSeason7 '
          }
          if (item.title[0].indexOf('my little pony movie|my little pony: the movie') > -1) {
            twtprefix = twtprefix + '#MyLittlePonyMovie '
          }

          // People were asking from wich episodes it came from.
          // Here's more info for you (only works with "spoilers:sXXeXX")
          var reep = /spoiler:([se0-9]*)/g
          var m
          while ((m = reep.exec(item.title[0])) !== null) {
            if (m.index === reep.lastIndex) reep.lastIndex++
            // Result may be empty if it's "spoilers:my little movie" for example
            if (m[1] || m[1].length !== 0) twtprefix = twtprefix + '(' + m[1] + ') '
          }

          // Here's our tweet message, and we're going to post it
          var tweet = twtprefix + '\n' + postTitle + ' - ' + item.link[0]
          console.log('Sending Tweet "' + tweet + '"')
          twitter.post('statuses/update',
            {status: tweet, media_ids: [data.media_id_string]},
            function (err, data, response) {
              // If something wrong here, it's bad luck
              if (err) {
                console.log('Error with ' + postTitle + ': ' + err)
              } else {
                // Yay, sent! Let's delete that temporarly downloaded file
                console.log('[' + new Date().toISOString() + '] "' + tweet)
                fs.unlink('pics/' + imgName, function (err) {
                  if (err) throw err
                })
              }
            }
          )
        }
      }
    )
  })
}

// Our real code
// Let's get our RSS XML
console.log('Starting...')
request({uri: rssUrl}, function (err, response, body) {
  // Things may go wrong. And it's probably your fault
  if (err && response.statusCode !== 200) {
    console.log('Request error when loading RSS feed')
  } else {
    // If we got our RSS XML, let's parse it
    console.log('Parsing Feed...')
    parseString(body, function (err, result) {
      // Really?
      if (err) {
        console.log('Error on parsing:' + err)
      } else {
        // Let's get all the items and prepare an array that acts like a queue
        var items = result.rss.channel[0].item
        var itemsToPublish = []

        console.log('Checking new content...')
        // So, for each items...
        for (var key in items) {
          // We get the published time
          var itemDate = new Date(Date.parse(items[key].pubDate))

          // And before to test it, let's check nothing's broken
          if (!config.latest || config.latest === 'Invalid Date') {
            console.log('latest posting date is invalid! Trying to fix it')
            // Let's just get the value of the current item
            // Bad news: we're skipping the current element for the cause
            config.latest = itemDate
          }

          // If the current item is more recent than the last checked one
          if (itemDate > new Date(Date.parse(config.latest))) {
            // Add it to our queue
            itemsToPublish.push(items[key])
          }
        }

        // Let's sort them by date (RSS may not be in correct order)
        itemsToPublish.sort(compareDates)

        // If there's nothing, warn us
        if (itemsToPublish.length === 0) console.log('Nothing to post!')

        // Let's start publishing our content!
        for (var i in itemsToPublish) {
          publishToTwitter(itemsToPublish[i])
          // Also save the last published date at each item.
          // I'm doing this every time after pubslishToTwitter ended,
          // to be sure it has been done
          config.latest = itemsToPublish[i].pubDate
          fs.writeFileSync('config.json', JSON.stringify(config, null, 2))
        }
      }
    })
  }
})
